<?php
class Usuario extends Conexion
{
  private $nombre;
  private $apellidos;
  private $telefono;
  private $curso;
  function __construct(){
    $this->conexion();
  }
  public function user(){
    $res=$this->conexion->query("SELECT nombre FROM usuarios WHERE id=1");
    return $res;
  }
  public function tabla() {
  $res=$this->conexion->query("SELECT * FROM usuarios");
  return $res;
  }
  public function comprobarCampos($post){
    $error=null;
    if(!isset($post)||!isset($post["nombre"])||!isset($post["apellidos"])||!isset($post["telefono"])||!isset($post["curso"])){
      $error="";
    }elseif($post["nombre"]==""){
      $error="No has introducido el nombre";
    }elseif($post["apellidos"]==""){
      $error="No has introducido los apellidos";
    }elseif($post["telefono"]==""){
      $error="No has introducido el telefono";
    }elseif($post["curso"]==""){
      $error="No has introducido el curso";
    }else{
      $error=false;
      $this->nombre=$post["nombre"];
      $this->apellidos=$post["apellidos"];
      $this->telefono=$post["telefono"];
      $this->curso=$post["curso"];
    }
    return $error;
  }
  public function registrarse(){
      $consulta="INSERT INTO usuarios (nombre, apellidos, telefono, curso) VALUES ('$this->nombre', '$this->apellidos', '$this->telefono', '$this->curso')";
      $this->resultado=$this->conexion->query($consulta);
      if(!$this->resultado) echo "$conexion->error";
      $this->registro="Registro satisfactorio";
      header("refresh:2");
  }
}
?>
